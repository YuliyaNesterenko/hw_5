package lesson5;

import java.util.Scanner;

public class HW_5 {
    public static void main(String[] args) {

        System.out.println("Введите выражение:");
        Scanner sc = new Scanner(System.in);
        String calc = sc.nextLine();

        String calculator = calc.replaceAll(" ", "");
        String operator = calculator.replaceAll("[1234567890]", "");

        int indexOperator = calculator.indexOf(operator);

        String firstPart = calculator.substring(0, indexOperator);
        String secondPart = calculator.substring(indexOperator + 1);

        if (operator.length() > 1) {
            System.out.println("Введены неверные данные");

        } else {
            int number1 = Integer.valueOf(firstPart);
            int number2 = Integer.valueOf(secondPart);
            System.out.println("Результат: ");
            switch (operator) {
                case "+":
                    System.out.println(number1 + number2);
                    break;
                case "-":
                    System.out.println(number1 - number2);
                    break;
                case "*":
                    System.out.println(number1 * number2);
                    break;
                case "/":
                    System.out.println((double) number1 / (double) number2);
                    break;
                default:
                    System.out.println("Введены неверные данные");

            }
        }
    }
}
